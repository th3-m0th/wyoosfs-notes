## Address space:
* Is a set of addresses that a process can use to access or allocate memory.
* 64-bit
* Byte Addressing

## Process
Is a program in execution, it contains program instructions, the data the program needs, heap and stack within the **address space**.
* Each process has its address space which process can read and write.
* Processes also contain other resources such as registers, list of open files, etc. A process is like a container holding all the information in order to run the program.
* The memory reference within one process does not affect the address space of other processes. 
* The kernel includes the essential information about each process,  such as process state. User programs are not allowed to access data in kernel space.

### Operation mode
There are two modes of operations kernel mode and user mode. The OS runs in kernel mode, in this mode we can execute all the instructions and access all the hardware. 
The application programs run in user mode where they can only execute a subset of the instructions and they cannot access data in the kernel space. 
If a program wants to access data in kernel or hardware it calls a special set of functions called system calls.
The **system call** acts as an interface between user programs and the operating system. When a program calls a system call, the control is transferred to the OS.
Then the OS performs the operations and returns the result to the program.