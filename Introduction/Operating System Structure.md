There are two common structures:
* Monolithic System
* Microkernel

An operating system contains scheduling, memory management, system call interface, scheduling, memory management, device drivers, etc...

## Monolithic Structure
The entire OS runs as a large executable program in kernel mode. In this structure, calling a function is simple, all we have to do is to find the correct function we need and call it. This would mean direct communication between modules and calling any function would be efficient.
This approach would make the system difficult to maintain and if a module has errors it will affect the whole system.

![[Monolithic_Structure.png]]


## Microkernel
In this structure the kernel is divided into small modules and one of them is User mode.